<?php
return [
    'BE' => [
        'compressionLevel' => '0',
        'debug' => '1',
        'explicitADmode' => 'explicitAllow',
        'installToolPassword' => '$2y$12$V.zfCY68.9JjR.Z7Qv0GnOAor1r/4JLDvuTy/SO4wYkGlKKcOetim',
        'loginSecurityLevel' => 'normal',
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\BcryptPasswordHash',
            'options' => [],
        ],
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8',
                'dbname' => 'db00095374',
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'password' => 'Z9r7870b6eucdCkd81mN',
                'port' => 3306,
                'user' => 'dbo00095374',
            ],
        ],
    ],
    'EXT' => [],
    'EXTCONF' => [
        'helhum-typo3-console' => [
            'initialUpgradeDone' => '10.4',
        ],
        'lang' => [
            'availableLanguages' => [
                'de',
            ],
        ],
    ],
    'EXTENSIONS' => [
        'autoloader' => [
            'enableAutoloaderClearCacheInProduction' => '0',
            'enableLanguageFileOnTableBase' => '0',
            'smartObjectClassLoadingIgnorePattern' => '',
        ],
        'backend' => [
            'backendFavicon' => '',
            'backendLogo' => '',
            'loginBackgroundImage' => 'fileadmin/mappenvorbereitungskurs/ueber-uns/header-ueber-uns/akademie-ruhr.jpg',
            'loginFootnote' => '',
            'loginHighlightColor' => '#717b83',
            'loginLogo' => 'EXT:akademie_ruhr/Resources/Public/Images/logo.svg',
        ],
        'calendarize' => [
            'addIndexInSpeakingUrl' => '0',
            'disableDateInSpeakingUrl' => '0',
            'disableDefaultEvent' => '0',
            'frequencyLimitPerItem' => '300',
            'respectTimesInTimeFrameConstraints' => '0',
            'tillDays' => '',
            'tillDaysPast' => '',
            'tillDaysRelative' => '',
            'timeShift' => '0',
        ],
        'cs_seo' => [
            'cropDescription' => '0',
            'evaluationDoktypes' => '1',
            'evaluators' => 'Title,Description,H1,H2,Images,Keyword',
            'inPageModule' => '0',
            'maxDescription' => '156',
            'maxH2' => '6',
            'maxNavTitle' => '50',
            'maxTitle' => '57',
            'minDescription' => '140',
            'minTitle' => '40',
            'modFileColumns' => 'title,description',
            'useAdditionalCanonicalizedUrlParametersOnly' => '0',
        ],
        'extensionmanager' => [
            'automaticInstallation' => '1',
            'offlineMode' => '0',
        ],
        'news' => [
            'advancedMediaPreview' => '1',
            'archiveDate' => 'date',
            'categoryBeGroupTceFormsRestriction' => '0',
            'categoryRestriction' => '',
            'contentElementPreview' => '1',
            'contentElementRelation' => '1',
            'dateTimeNotRequired' => '0',
            'hidePageTreeForAdministrationModule' => '0',
            'manualSorting' => '0',
            'mediaPreview' => 'false',
            'prependAtCopy' => '1',
            'resourceFolderImporter' => '/news_import',
            'rteForTeaser' => '0',
            'showAdministrationModule' => '1',
            'showImporter' => '0',
            'slugBehaviour' => 'unique',
            'storageUidImporter' => '1',
            'tagPid' => '1',
        ],
        'scheduler' => [
            'maxLifetime' => '1440',
            'showSampleTasks' => '1',
        ],
        'vhs' => [
            'disableAssetHandling' => '0',
        ],
        'youtubevideo' => [
            'youtubevideoEnableBePlayer' => '1',
            'youtubevideoEnablePagination' => '0',
        ],
    ],
    'FE' => [
        'cacheHash' => [
            'excludedParameters' => [
                'L',
                'pk_campaign',
                'pk_kwd',
                'utm_source',
                'utm_medium',
                'utm_campaign',
                'utm_term',
                'utm_content',
                'gclid',
                'fbclid',
            ],
        ],
        'compressionLevel' => 0,
        'debug' => '1',
        'disableNoCacheParameter' => true,
        'loginSecurityLevel' => 'normal',
        'pageNotFoundOnCHashError' => false,
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\BcryptPasswordHash',
        ],
    ],
    'GFX' => [
        'jpg_quality' => 70,
        'processor' => 'GraphicsMagick',
        'processor_allowTemporaryMasksAsPng' => false,
        'processor_colorspace' => 'RGB',
        'processor_effects' => false,
        'processor_enabled' => true,
        'processor_path' => '/usr/bin/',
        'processor_path_lzw' => '/usr/bin/',
    ],
    'LOG' => [
        'TYPO3' => [
            'CMS' => [
                'deprecations' => [
                    'writerConfiguration' => [
                        5 => [
                            'TYPO3\CMS\Core\Log\Writer\FileWriter' => [
                                'disabled' => false,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'MAIL' => [
        'defaultMailFromAddress' => 'hallo@mappenvorbereitungskurs.de',
        'defaultMailFromName' => 'Akademie Ruhr',
        'defaultMailReplyToName' => 'Akademie Ruhr',
        'transport' => 'smtp',
        'transport_sendmail_command' => '/usr/sbin/sendmail -t -i ',
        'transport_smtp_encrypt' => true,
        'transport_smtp_password' => 'halloMVK+PB+?!PB',
        'transport_smtp_server' => 'smtp.strato.de:465',
        'transport_smtp_username' => 'hallo@mappenvorbereitungskurs.de',
    ],
    'SYS' => [
        'belogErrorReporting' => 0,
        'caching' => [
            'cacheConfigurations' => [
                'autoloader' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\SimpleFileBackend',
                    'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\PhpFrontend',
                    'groups' => [
                        'system',
                    ],
                    'options' => [
                        'defaultLifetime' => 0,
                    ],
                ],
                'extbase_object' => [
                    'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
                    'groups' => [
                        'system',
                    ],
                    'options' => [
                        'defaultLifetime' => 0,
                    ],
                ],
                'hash' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
                ],
                'imagesizes' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\SimpleFileBackend',
                    'options' => [
                        'compression' => '__UNSET',
                    ],
                ],
                'pages' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
                    'options' => [
                        'compression' => '__UNSET',
                    ],
                ],
                'pagesection' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
                    'options' => [
                        'compression' => '__UNSET',
                    ],
                ],
                'rootline' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\FileBackend',
                    'options' => [
                        'compression' => '__UNSET',
                    ],
                ],
            ],
        ],
        'devIPmask' => '*',
        'displayErrors' => '1',
        'encryptionKey' => 'f2205630a472ecfc585cff6444b52015c4d0399fb34eb1c6c83078f5b20878136d13d8d2744f5b0a8edbd54e10308d5d',
        'exceptionalErrors' => 12290,
        'features' => [
            'TypoScript.strictSyntax' => true,
            'redirects.hitCount' => false,
            'simplifiedControllerActionDispatching' => true,
            'unifiedPageTranslationHandling' => true,
        ],
        'ipAnonymization' => 2,
        'sitename' => 'Akademie Ruhr',
        'systemMaintainers' => [
            1,
        ],
    ],
];
