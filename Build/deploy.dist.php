<?php declare(strict_types=1);

namespace Deployer;

require 'recipe/typo3.php';

// TYPO3
desc('Finish TYPO3 Deployment');
task('typo3cms:install', function () {
    run('{{bin/php}} {{deploy_path}}/current/vendor/bin/typo3cms install:generatepackagestates');
    run('{{bin/php}} {{deploy_path}}/current/vendor/bin/typo3cms install:fixfolderstructure');
    // run('{{bin/php}} {{deploy_path}}/current/vendor/bin/typo3cms database:updateschema');
    run('{{bin/php}} {{deploy_path}}/current/vendor/bin/typo3cms cache:flush');
});

// Main
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:symlink',
    'typo3cms:install',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');
after('deploy', 'success');

// If deployment fails automatically unlock
after('deploy:failed', 'deploy:unlock');

// Configuration
set('application', 'Akademie Ruhr');
set('typo3_webroot', 'public');
set('allow_anonymous_stats', false);

// Hosts
host(getenv('SSH_HOST'))
    ->stage('production')
    ->set('repository', 'git@gitlab.com:agenturkim/akademie-ruhr/site.git')
    ->set('branch', 'master')
    ->user(getenv('SSH_USER'))
    ->port(22)
    ->set('bin/php', '/usr/bin/php74')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader')
    ->set('ssh_type', 'native')
    ->set('deploy_path', getenv('SSH_DEPLOY_PATH'));
