# Akademie Ruhr

> TYPO3

## Development Environment

### Prerequisites

- Install docker
- install ddev

### Creating a GitLab access token

Create a access token in GitLab for enabling composer to install shared packages and enable the scopes: read_api and read_repository.
Once created execute the following commands which will create a  auth.json file which contains the gitlab token.

```bash
$ ddev composer config gitlab-token.gitlab.com GITLAB_ACCESS_TOKEN
```

### Usage

- `ddev start` inside project directory to spin up the development environment
- `ddev sync-from-production` to copy over fileadmin, config and database from production (This assumes you have an entry "akademie" in your SSH Config)

## Releasing

- Manually deploy from Gitlab PRODUCTION Pipeline when commiting to master
